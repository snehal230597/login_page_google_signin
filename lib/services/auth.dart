import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:login_page/localdb.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

Future<User?> signInWithGoogle() async {
  final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
  final GoogleSignInAuthentication? googleSignInAuthentication =
      await googleSignInAccount?.authentication;

  final AuthCredential credential = GoogleAuthProvider.credential(
      idToken: googleSignInAuthentication!.idToken,
      accessToken: googleSignInAuthentication.accessToken);

  // ignore: non_constant_identifier_names
  final UserCredential = await _auth.signInWithCredential(credential);
  final User? user = UserCredential.user;

  assert(!user!.isAnonymous);
  assert(await user?.getIdToken(false) != null);

  final User? currentUser = _auth.currentUser;
  assert(currentUser?.uid == user?.uid);
  print(user);

  LocalDataSaver.savaLoginData(true);
  LocalDataSaver.saveName(user!.displayName.toString());
  LocalDataSaver.saveEmail(user.email.toString());
  LocalDataSaver.saveImg(user.photoURL.toString());

  return user;
}

  Future<String> signOut() async {
  await googleSignIn.signOut();
  await _auth.signOut();
  return "SUCCESS";
}
