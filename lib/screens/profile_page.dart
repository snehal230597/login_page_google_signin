import 'package:flutter/material.dart';
import 'package:login_page/constant.dart';
import 'package:login_page/screens/login_page.dart';
import 'package:login_page/services/auth.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  signOutMethod(context) async {
    await signOut();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => LoginPageScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(backgroundImage: NetworkImage(constant.img)),
              SizedBox(height: 10),
              Text(constant.name),
              Text(constant.email),
              SizedBox(height: 10),
              ElevatedButton(
                onPressed: () {
                  signOutMethod(context);
                },
                child: Text('Logout'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
