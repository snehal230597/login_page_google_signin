import 'package:flutter/material.dart';

class CustomField extends StatefulWidget {
  final String? label;
  final double? size;
  final double? fontSize;
  final FontWeight? fontWeight;
  final FontWeight? fontWeight1;
  final String? fontFamily;
  final FontStyle? fontStyle;
  final bool? obSecure;
  final Widget? suffixIcon;
  final TextInputType? keyboardType;
  final FormFieldValidator? fieldValidator;
  final TextInputAction? textInputAction;
  final TextEditingController? controller;

  CustomField(
      {this.label,
      this.size,
      this.fontWeight,
      this.fontWeight1,
      this.fontFamily,
      this.fontStyle,
      this.fontSize,
      this.obSecure,
      this.suffixIcon,
      this.textInputAction,
      this.fieldValidator,
      this.controller,
      this.keyboardType});

  @override
  _CustomFieldState createState() => _CustomFieldState();
}

class _CustomFieldState extends State<CustomField> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 5, left: 15, right: 15),
      child: Form(
        key: formKey,
        child: TextFormField(
          validator: widget.fieldValidator,
          controller: widget.controller,
          textInputAction: widget.textInputAction,
          style: TextStyle(
              fontSize: widget.fontSize, fontWeight: widget.fontWeight1),
          obscureText: widget.obSecure!,
          keyboardType: widget.keyboardType,
          decoration: InputDecoration(
            suffixIcon: widget.suffixIcon,
            labelText: widget.label,
            labelStyle: TextStyle(
              fontSize: widget.size,
              fontWeight: widget.fontWeight,
              fontFamily: widget.fontFamily,
              fontStyle: widget.fontStyle,
            ),
          ),
        ),
      ),
    );
  }
}
