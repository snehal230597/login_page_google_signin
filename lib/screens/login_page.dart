import 'package:flutter/material.dart';
import 'package:login_page/constant.dart';
import 'package:login_page/localdb.dart';
import 'package:login_page/screens/common_button.dart';
import 'package:login_page/screens/common_field.dart';
import 'package:login_page/screens/common_text.dart';
import 'package:login_page/screens/profile_page.dart';
import 'package:login_page/screens/stack.dart';
import 'package:login_page/services/auth.dart';

class LoginPageScreen extends StatefulWidget {
  @override
  _LoginPageScreenState createState() => _LoginPageScreenState();
}

class _LoginPageScreenState extends State<LoginPageScreen> {
  signInMethod(context) async {
    await signInWithGoogle();
    constant.name = (await LocalDataSaver.getName())!;
    constant.email = (await LocalDataSaver.getEmail())!;
    constant.img = (await LocalDataSaver.getImg())!;
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => ProfilePage(),
      ),
    );
  }

  bool _isObscure = true;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        reverse: true,
        child: Padding(
          padding:
              EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          child: SafeArea(
            child: Column(
              children: [
                MainImage(),
                CustomField(
                  fontSize: 16,
                  controller: emailController,
                  keyboardType: TextInputType.emailAddress,
                  textInputAction: TextInputAction.go,
                  fontWeight1: FontWeight.w600,
                  label: "Email",
                  fieldValidator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter email id';
                    }
                    return null;
                  },
                  size: 16,
                  fontWeight: FontWeight.w400,
                  fontFamily: "SegoeUI",
                  fontStyle: FontStyle.normal,
                  obSecure: false,
                ),
                CustomField(
                  fontSize: 20,
                  textInputAction: TextInputAction.done,
                  fontWeight1: FontWeight.w600,
                  label: "Password",
                  controller: passwordController,
                  fieldValidator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Please enter password';
                    }
                    return null;
                  },
                  keyboardType: TextInputType.number,
                  size: 16,
                  fontWeight: FontWeight.w400,
                  fontFamily: "SegoeUI",
                  fontStyle: FontStyle.normal,
                  suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _isObscure = !_isObscure;
                        });
                      },
                      icon: Icon(_isObscure
                          ? Icons.visibility
                          : Icons.visibility_off)),
                  obSecure: _isObscure,
                ),
                InkWell(
                  onTap: () {
                    setState(() {
                      print('Sign in button tapped');
                      if (formKey.currentState!.validate()) {}
                    });
                  },
                  child: AllButtons(
                    height: 50,
                    width: 370,
                    decorationColor: Colors.blue,
                    borderRadius: BorderRadius.circular(27),
                    lableText: "Sign In",
                    fontfamily: 'SegoeUI',
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w600,
                    size: 17,
                    textColor: Colors.white,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 08, left: 200),
                  child: InkWell(
                    onTap: () {
                      print('Forgot button Tapped!');
                    },
                    child: AllText(
                      textString: "Forgot Password?",
                      textFamily: "SegoeUI",
                      textColor: Colors.black38,
                      textSize: 15,
                      textWeight: FontWeight.w600,
                      textStyle: FontStyle.normal,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 08),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: 85,
                        height: 2.0,
                        decoration: new BoxDecoration(
                          color: Color(0xffc7c7c7),
                        ),
                      ),
                      SizedBox(width: 15),
                      AllText(
                        textString: "OR",
                        textFamily: 'SegoeUI',
                        textColor: Color(0xff2e3755),
                        textSize: 17,
                        textWeight: FontWeight.w600,
                        textStyle: FontStyle.normal,
                      ),
                      SizedBox(width: 15),
                      Container(
                        width: 85,
                        height: 2.0,
                        decoration: new BoxDecoration(
                          color: Color(0xffc7c7c7),
                        ),
                      ),
                    ],
                  ),
                ),
                // Form(
                //   key: formKey,
                //   child: Padding(
                //     padding: const EdgeInsets.all(20),
                //     child: Center(
                //       child: TextFormField(
                //         validator: (value) {
                //           if (value == null || value.isEmpty) {
                //             return 'Please enter email id';
                //           }
                //           return null;
                //         },
                //         decoration: InputDecoration(
                //           hintText: 'Email',
                //           prefixIcon: Icon(Icons.email),
                //         ),
                //       ),
                //     ),
                //   ),
                // ),
                // SignInWithGoogle(),
                Padding(
                  padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                  child: InkWell(
                    onTap: () {
                      signInMethod(context);
                    },
                    child: Container(
                      height: 50,
                      width: 370,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.blue, width: 2),
                          borderRadius: BorderRadius.circular(27)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            'assets/images/google_icon.png',
                            height: 30,
                          ),
                          SizedBox(width: 10),
                          Text(
                            'Sign in with Google',
                            style: TextStyle(
                              fontFamily: 'SegoeUI',
                              color: Colors.black,
                              fontSize: 17,
                              fontWeight: FontWeight.w600,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
